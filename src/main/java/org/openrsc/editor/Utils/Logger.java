package org.openrsc.editor.Utils;
/*
  rscplus

  <p>This file is part of rscplus.

  <p>rscplus is free software: you can redistribute it and/or modify it under the terms of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  <p>rscplus is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
  even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  <p>You should have received a copy of the GNU General Public License along with rscplus. If not,
  see <http://www.gnu.org/licenses/>.

  <p>Authors: see <https://github.com/RSCPlus/rscplus>
 */


import org.openrsc.editor.Settings;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A simple logger
 */
public class Logger {
    private static PrintWriter m_logWriter;
    private static int levelFixedWidth = 0;
    private static String m_uncoloredMessage = "";

    public enum Type {
        ERROR(0, "error", true, true),
        WARN(1, "warn", true, true),
        INFO(3, "info", true, true),
        DEBUG(4, "debug", true, true);

        Type(int id, String name, boolean showLevel, boolean showTimestamp) {
            this.id = id;
            this.name = name;
            this.showLevel = showLevel;
            this.showTimestamp = showTimestamp;

            levelFixedWidth = Math.max(levelFixedWidth, name.length());
        }

        public final int id;
        public final String name;
        public final boolean showLevel;
        public final boolean showTimestamp;
    }

    public static void start() {
        File file = new File("log.txt");
        try {
            m_logWriter = new PrintWriter(new FileOutputStream(file));
        } catch (Exception ignored) {
        }
    }

    public static void stop() {
        try {
            m_logWriter.close();
        } catch (Exception ignored) {
        }
    }

    public static void Log(Type type, String message) {
        try {
            if (type.id > Settings.logVerbosity || message == null) return;

            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            String msg = message;
            String extra = "";

            if (type.showLevel) {
                // Uppercase and pad level for monospace fonts
                String levelText = type.name.toUpperCase();
                while (levelText.length() < levelFixedWidth) levelText = " " + levelText;

                extra += "[" + levelText + "]";
            }
            if (type.showTimestamp) {
                extra += "[" + dateFormat.format(new Date()) + "]";
            }

            if (extra.length() > 0) msg = extra + " " + msg;

            if (type != Type.ERROR) System.out.println(msg);
            else System.err.println(msg);

            try {
                if (m_uncoloredMessage.length() > 0) {
                    msg = m_uncoloredMessage;
                    m_uncoloredMessage = "";
                }

                // Output to log file
                m_logWriter.write(msg + "\r\n");
                m_logWriter.flush();
            } catch (Exception ignored) {
            }
        } catch (Exception e) {
            try {
                System.out.println("Logger died, heres the report:");
                e.printStackTrace();
            } catch (Exception ignored) {
            }
        }
    }

    // String variants

    public static void Error(String message) {
        Log(Type.ERROR, message);
    }

    public static void Warn(String message) {
        Log(Type.WARN, message);
    }

    public static void Info(String message) {
        Log(Type.INFO, message);
    }

    public static void Debug(String message) {
        Log(Type.DEBUG, message);
    }

}
