package org.openrsc.editor.gui.menu;

import org.openrsc.editor.Actions;
import org.openrsc.editor.SelectSection;
import org.openrsc.editor.Settings;
import org.openrsc.editor.gui.GuiUtils;
import smile.swing.FileChooser;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.util.function.Consumer;

public class FileMenu extends BaseMenu {

    public FileMenu(
            Consumer<File> onOpenLandscape,
            Runnable onOpenSection,
            Runnable onSaveLandscape,
            Runnable onRevertLandscape,
            Runnable onExit
    ) {
        super("File");

        JMenuItem openDataDir = new JMenuItem();
        openDataDir.setText("Open NPC/item/scenery location directory");
        openDataDir.addActionListener(evt -> {
            final JFileChooser fc;
            if (Settings.cacheDir != null) {
                fc = new JFileChooser(Settings.cacheDir);
            } else {
                fc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
            }
            fc.setFileSelectionMode(FileChooser.DIRECTORIES_ONLY);
            fc.setDialogTitle("Locate the \"Cache\\video\" folder");
            if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                FileMenu.setLastDir(fc.getSelectedFile());
                Actions.onOpenDataDir(fc.getSelectedFile());
                Settings.cacheDir = String.valueOf(fc.getSelectedFile());
                Settings.saveSettings();
            }
        });
        add(openDataDir);

        JMenuItem openLandscape = new JMenuItem();
        openLandscape.setText("Open \"Custom_Landscape.orsc\"");
        openLandscape.addActionListener(evt -> {

            final JFileChooser fc;
            if (Settings.cacheDir != null) {
                fc = new JFileChooser(Settings.cacheDir);
            } else {
                fc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
            }
            fc.setDialogTitle("Locate \"Custom_Landscape.orsc\"");
            if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                onOpenLandscape.accept(fc.getSelectedFile());
                FileMenu.setLastDir(fc.getSelectedFile());
                SelectSection ss = new SelectSection();
                ss.setVisible(true);
                openDataDir.setEnabled(false);
            }
        });
        add(openLandscape);

        JMenuItem saveLandscape = new JMenuItem();
        saveLandscape.setText("Save \"Custom_Landscape.orsc\"");
        saveLandscape.addActionListener(GuiUtils.fromRunnable(onSaveLandscape));
        add(saveLandscape);

        JMenuItem revertLandscape = new JMenuItem();
        revertLandscape.setText("Revert landscape");
        revertLandscape.addActionListener(GuiUtils.fromRunnable(onRevertLandscape));
        add(revertLandscape);

        JMenuItem openSection = new JMenuItem();
        openSection.setText("Open section");
        openSection.addActionListener(GuiUtils.fromRunnable(onOpenSection));
        add(openSection);

        addSeparator();

        JMenuItem exit = new JMenuItem();
        exit.setText("Exit");
        exit.addActionListener(GuiUtils.fromRunnable(onExit));
        add(exit);
    }

    public static void setLastDir(File file) {
        Settings.cacheDir = file.getParent();
    }
}

